package com.curso.mecademia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private EditText etUsuario, etPassword;
    private Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUsuario = findViewById(R.id.etUsuario);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
    }

    public void login(View view) {
        //etUsuario.findFocus();
        String usuario = etUsuario.getText().toString();
        String password = etPassword.getText().toString();
        if(TextUtils.isEmpty(usuario) || TextUtils.isEmpty(password) ){
            Toast.makeText(this,"Debe ingresar todos los campos",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if(usuario.equals("admin") && password.equals("admin")){
            Toast.makeText(this,"Usuario Correcto",
                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,ProductosActivity.class);
            intent.putExtra("usuario",usuario);
            intent.putExtra("passwor",password);
            startActivity(intent);
            finish();
        }
        else {
            Toast.makeText(this,"Usuario y/o contraseña incorrectos",
                    Toast.LENGTH_SHORT).show();
            return;
        }

    }
}
