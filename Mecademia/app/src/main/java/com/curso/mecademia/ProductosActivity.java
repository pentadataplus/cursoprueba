package com.curso.mecademia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ProductosActivity extends AppCompatActivity {
    private TextView tvDataUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);
        tvDataUser = findViewById(R.id.tvDataUser);
        Intent intent = getIntent();
        String usuario = intent.getStringExtra("usuario");
        String password = intent.getStringExtra("password");
        tvDataUser.setText(usuario);

    }
}
